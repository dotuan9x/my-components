// libraries
import React, {useState, useEffect} from 'react';

// Components
import Comments from 'Components/comments';

// Services
import * as CommentServices from 'Services/Comment';

// Assets
import 'Assets/css/styles.scss';

// Utils
import {handleError} from 'Src/handleError';

function App() {
    const [theme, setTheme] = useState('dark');
    const [isShowLoading, setShowLoading] = useState(true);
    const [comments, setComments] = useState([]);

    useEffect(() => {
        try {
            // Get list comments
            const getListComment = CommentServices.getList();

            if (getListComment) {
                getListComment.then((response) => {
                    setShowLoading(false);

                    if (response && response.data) {
                        setComments(response.data);
                    }
                });
            }
        } catch (error) {
            // Error
            handleError(error);
        }
    }, []);

    useEffect(() => {
        // Set theme default
        document.documentElement.setAttribute('theme', theme);
    }, [theme]);

    const onReply = (comment) => {
        // Create comment
        CommentServices.create(comment);
    };

    const onClickSwitchThemeIcon = () => {
        const mode = theme === 'light' ? 'dark' : 'light';

        setTheme(mode);
    };

    const renderSwitchThemeIcon = () => {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                style={{transform: 'rotate(40deg)', cursor: 'pointer'}}
                onClick={onClickSwitchThemeIcon}
            >
                {
                    theme === 'light' ? (
                        <>
                            <mask id="mask">
                                <rect x="0" y="0" width="100%" height="100%" fill="white" />
                                <circle cx="12" cy="4" r="9" fill="black" />

                            </mask>
                            <circle fill="black" cx="12" cy="12" r="9" mask="url(#mask)" />
                        </>
                    ) : null
                }

                {
                    theme === 'dark' ? (
                        <>
                            <circle fill="black" cx="12" cy="12" r="5" />
                            <g stroke="currentColor">
                                <line x1="12" y1="1" x2="12" y2="3" />
                                <line x1="12" y1="21" x2="12" y2="23" />
                                <line x1="4.22" y1="4.22" x2="5.64" y2="5.64" />
                                <line x1="18.36" y1="18.36" x2="19.78" y2="19.78" />
                                <line x1="1" y1="12" x2="3" y2="12" />
                                <line x1="21" y1="12" x2="23" y2="12" />
                                <line x1="4.22" y1="19.78" x2="5.64" y2="18.36" />
                                <line x1="18.36" y1="5.64" x2="19.78" y2="4.22" />
                            </g>
                        </>
                    ) : null
                }
            </svg>
        );
    };

    const renderLoadingIcon = () => {
        return (
            <div className="comment-feature-loading">
                <span>
                    <svg width="38" height="38" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#fff">
                        <g fill="none" fillRule="evenodd">
                            <g transform="translate(1 1)" strokeWidth="2">
                                <circle strokeOpacity=".5" cx="18" cy="18" r="18" />
                                <path d="M36 18c0-9.94-8.06-18-18-18">
                                    <animateTransform
                                        attributeName="transform"
                                        type="rotate"
                                        from="0 18 18"
                                        to="360 18 18"
                                        dur="1s"
                                        repeatCount="indefinite" />
                                </path>
                            </g>
                        </g>
                    </svg>
                </span>
            </div>
        );
    };

    return (
        <div className="comment-feature">
            <div className="switch-theme">
                <span>Switch theme to: </span>
                <a title="Click to switch theme">
                    {renderSwitchThemeIcon()}
                </a>
            </div>
            <div className="comment-feature-block">
                {
                    isShowLoading ? renderLoadingIcon() :
                        <Comments
                            username={'Tuan Do'}
                            avatar={'https://avatars.githubusercontent.com/u/9269487?v=4'}
                            comments={comments}
                            onReply={onReply}
                        />
                }
            </div>
        </div>
    );
}

export default App;
