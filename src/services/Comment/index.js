import {services} from 'Services/services';
import {appConfig} from 'Src/constant';

export function getList(params) {
    return services.getList({
        ...params,
        API_HOST: appConfig.API_HOST + 'comments'
    });
}

export function create(params) {
    return services.create({
        ...params,
        API_HOST: appConfig.API_HOST + 'comments'
    });
}
