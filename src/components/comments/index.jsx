// Libraries
import React, {useState, useEffect, useRef} from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import moment from 'moment';

// Assets
import styles from './styles.module.scss';

// Utils
import {splitStringWithLimitWord, formatDateTimeToShow} from './utils';

const defaultProps = {
    width: 680,
    username: 'Tuan Do',
    avatar: 'https://avatars.githubusercontent.com/u/9269487?v=4',
    numberCommentShow: 2, // Number comment parent show the first time
    numberReplyPerComment: 2, // Default max number reply show into the comment.
    comments: [
        {
            id: 1,
            username: 'Tuan Do',
            avatar: 'https://avatars.githubusercontent.com/u/9269487?v=4',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting',
            datetime: '2021-03-24 15:30:00',
            parentId: 0
        },
        {
            id: 2,
            username: 'Tuan Do',
            avatar: 'https://avatars.githubusercontent.com/u/9269487?v=4',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ',
            datetime: '2021-03-26 15:30:00',
            parentId: 0
        },
        {
            id: 3,
            username: 'Tuan Do',
            avatar: 'https://avatars.githubusercontent.com/u/9269487?v=4',
            text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ',
            datetime: '2021-03-26 15:30:00',
            parentId: 1
        }
    ],
    onReply: () => {},
    onError: () => {}
};

const MAX_NUMBER_WORD_PER_COMMENT = 50; // To show see more
const KEY_CODE_ENTER = 13;

const Comments = (props) => {
    const [isShowViewPrevious, setShowViewPrevious] = useState(false);
    const [allComments, setAllComments] = useState(props.comments);
    const [comments, setComments] = useState([]);
    const myRef = useRef(null);

    useEffect(() => {
        try {
            if (props.comments.length) {
                let arrComments = [];
                let arrReply = [];

                props.comments.map((comment) => {
                    if (comment.id && comment.username && comment.avatar && comment.text) {
                        const formatted = formatComment(comment);

                        if (formatted) {
                            if (+comment.parentId) {
                                if (arrReply[comment.parentId]) {
                                    arrReply[comment.parentId].push(formatted);
                                } else {
                                    arrReply[comment.parentId] = [formatted];
                                }
                            } else {
                                arrComments.push(formatted);
                            }
                        }
                    }
                });

                // Add reply of comment to parent
                if (arrComments.length && arrReply.length) {
                    arrComments.map((comment) => {
                        if (arrReply[comment.id]) {
                            let arrReplyByComment = [].concat(arrReply[comment.id]);

                            if (arrReplyByComment && arrReplyByComment.length) {
                                if (arrReplyByComment.length > props.numberReplyPerComment) {
                                    comment.moreReplyNumber = arrReplyByComment.length - props.numberReplyPerComment;
                                }

                                comment.reply = arrReplyByComment.slice(0, props.numberReplyPerComment);
                                comment.allReply = [].concat(arrReplyByComment);
                            }
                        }
                    });
                }

                setAllComments(arrComments);
            }
        } catch (error) {
            handleError(error);
        }
    }, [props.comments]);

    useEffect(() => {
        try {
            if (allComments.length && !comments.length) {
                let arrAllComments = [].concat(allComments);

                // Reverse list comments, latest comment show final
                arrAllComments.reverse();

                // Handle list comments show
                if (arrAllComments.length > props.numberCommentShow) {
                    const arrComments = arrAllComments.slice(0, props.numberCommentShow);

                    arrComments.reverse();

                    setComments(arrComments);
                    setShowViewPrevious(true);
                } else {
                    setComments(arrAllComments);
                }
            }
        } catch (error) {
            handleError(error);
        }
    }, [allComments]);

    useEffect(() => {
        try {
            if (comments.length === allComments.length) {
                setShowViewPrevious(false);
            }
        } catch (error) {
            handleError(error);
        }
    }, [comments]);

    const formatComment = (comment) => {
        try {
            let isShowSeeMore = false;

            if (comment.text) {
                const splitText = comment.text.split(' ');

                if (splitText && splitText.length > MAX_NUMBER_WORD_PER_COMMENT) {
                    isShowSeeMore = true;
                }
            }

            return {
                id: +comment.id,
                username: comment.username,
                avatar: comment.avatar,
                text: comment.text,
                time: formatDateTimeToShow(comment.datetime),
                parentId: +comment.parentId || 0,
                reply: comment.reply || [],
                allReply: comment.allReply || [],
                isShowReplyInput: false,
                isShowSeeMore: isShowSeeMore,
                moreReplyNumber: 0
            };
        } catch (error) {
            // Error
            handleError(error);
        }
    };

    const onClickViewPreviousComments = () => {
        try {
            if (allComments.length) {
                let arrAllComments = [].concat(allComments);

                // Reverse list comments, latest comment show final
                arrAllComments.reverse();

                let arrComments = [];

                arrAllComments.map((comment) => {
                    let isExists = comments.find((item) => item.id === comment.id);

                    if (!isExists) {
                        arrComments.push(comment);
                    }
                });

                if (arrComments.length) {
                    setComments(arrComments.concat(comments));
                }
            }
        } catch (error) {
            handleError(error);
        }
    };

    const onClickReply = (replyCommentId) => {
        try {
            if (replyCommentId && comments.length) {
                const arrComments = comments.map((comment) => {
                    if (+comment.id === +replyCommentId) {
                        comment.isShowReplyInput = true;
                    }

                    return comment;
                });

                setComments(arrComments);
            }

            // Focus input for reply
            if (myRef.current) {
                setTimeout(() => {
                    const inputElem = myRef.current.getElementsByClassName('reply-input-' + replyCommentId);

                    if (inputElem.length) {
                        inputElem[0].focus();
                    }
                }, 100);
            }
        } catch (error) {
            // Error
            handleError(error);
        }
    };

    const onClickViewMoreReply = (viewMoreReplyComment) => {
        try {
            if (viewMoreReplyComment && viewMoreReplyComment.id && viewMoreReplyComment.allReply && comments.length) {
                const arrComments = comments.map((comment) => {
                    if (comment.id === viewMoreReplyComment.id) {
                        comment.reply = viewMoreReplyComment.allReply;
                        comment.moreReplyNumber = 0;
                    }

                    return comment;
                });

                setComments(arrComments);
            }
        } catch (error) {
            handleError(error);
        }
    };

    const onClickSeeMore = (seeMoreComment) => {
        try {
            if (seeMoreComment) {
                if (!seeMoreComment.parentId) {
                    const arrComments = comments.map((comment) => {
                        if (comment.id === seeMoreComment.id) {
                            comment.isShowSeeMore = false;
                        }

                        return comment;
                    });

                    setComments(arrComments);
                } else {
                    const arrComments = comments.map((comment) => {
                        if (comment.reply && comment.reply.length) {
                            comment.reply = comment.reply.map((replyComment) => {
                                if (replyComment.id === seeMoreComment.id) {
                                    replyComment.isShowSeeMore = false;
                                }

                                return replyComment;
                            });
                        }

                        return comment;
                    });

                    setComments(arrComments);
                }
            }
        } catch (error) {
            // Error
            handleError(error);
        }
    };

    const onMouseEnterReplyInput = (event, replyCommentId) => {
        try {
            const text = event.target.value;

            if (event.charCode === KEY_CODE_ENTER && typeof props.onReply === 'function') {
                event.target.value = '';

                const comment = {
                    id: props.comments.length + 1,
                    username: props.username,
                    avatar: props.avatar,
                    text,
                    datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
                    parentId: replyCommentId
                };

                props.onReply(comment);

                // Add comment to all comments + list comments
                let arrAllComments = [].concat(allComments);
                let arrComments = [].concat(comments);
                let formatted = formatComment(comment);

                if (!replyCommentId) {
                    // Comment
                    arrAllComments.push(formatted);
                    arrComments.push(formatted);
                } else {
                    // Reply
                    arrComments.map((comment) => {
                        if (+comment.id === +replyCommentId) {
                            let arrReply = [].concat(comment.reply);
                            let arrAllReply = [].concat(comment.allReply);

                            arrReply.push(formatted);
                            arrAllReply.push(formatted);

                            comment.reply = arrReply;
                            comment.allReply = arrAllReply;
                        }
                    });
                }

                setAllComments(arrAllComments);
                setComments(arrComments);
            }
        } catch (error) {
            handleError(error);
        }
    };

    const renderComments = (comments, parentId = 0) => {
        try {
            if (comments && comments.length) {
                return (
                    <ul>
                        {
                            comments ? comments.map((comment) => {
                                const replyCommentId = parentId <= 0 ? comment.id : parentId;

                                return (
                                    <li key={comment.id.toString()}>
                                        <div className={classnames(styles['user-comment'])}>
                                            <div className={classnames(styles['avatar'])}>
                                                <img alt={comment.username} src={comment.avatar} />
                                            </div>
                                            <div className={classnames(styles['detail'])}>
                                                <div className={classnames(styles['comment-content'])}>
                                                    <div className={classnames(styles['username'])}>
                                                        <a>{comment.username}</a>
                                                    </div>
                                                    <div className={classnames(styles['content'])}>
                                                        {
                                                            comment.isShowSeeMore ? splitStringWithLimitWord(comment.text, MAX_NUMBER_WORD_PER_COMMENT) + '...' : comment.text
                                                        }
                                                    </div>
                                                    {
                                                        comment.isShowSeeMore ? (
                                                            <span onClick={() => onClickSeeMore(comment)} className={classnames(styles['see-more'])}><a>See More</a></span>
                                                        ) : null
                                                    }
                                                </div>
                                                <div className={classnames(styles['comment-settings'])}>
                                                    <span onClick={() => onClickReply(replyCommentId)} className={classnames(styles['reply'])}>
                                                        <a>Reply</a>
                                                    </span>
                                                    <span> · </span>
                                                    <span className={classnames(styles['time'])}>
                                                        <a>{comment.time}</a>
                                                    </span>
                                                </div>
                                            </div>
                                            {
                                                comment.reply && comment.reply.length ? renderComments(comment.reply, comment.id) : null
                                            }
                                            {
                                                // Only view more reply for parent comment
                                                !comment.parentId && comment.moreReplyNumber > 0 ? (
                                                    <div className={classnames(styles['view-more-reply'])}>
                                                        <i className='icon-reply-dark' />
                                                        <span onClick={() => onClickViewMoreReply(comment)}>
                                                            <a>View {comment.moreReplyNumber} more reply</a>
                                                        </span>
                                                    </div>
                                                ) : null
                                            }

                                            {
                                                comment.isShowReplyInput && renderCommentInput(comment.id)
                                            }
                                        </div>
                                    </li>
                                );
                            }) : null
                        }
                    </ul>
                );
            }
        } catch (error) {
            // Handle error
            handleError(error);
        }
    };

    const renderCommentInput = (replyCommentId = '') => {
        try {
            return (
                <div className={classnames(styles['user-input'])}>
                    <div className={classnames(styles['avatar'])}>
                        <img alt={props.username} src={props.avatar} />
                    </div>
                    <div className={classnames(styles['input'])}>
                        <input onKeyPress={(e) => onMouseEnterReplyInput(e, replyCommentId)} className={'reply-input-' + replyCommentId} placeholder="Write a comment..." />
                    </div>
                </div>
            );
        } catch (error) {
            // Handle error
            handleError(error);
        }
    };

    const handleError = (error) => {
        if (typeof props.onError === 'function') {
            props.onError(error);
        }
    };

    return (
        <div ref={myRef} style={{width: props.width}} className={classnames(styles['wrapper'])}>
            {
                isShowViewPrevious ? (
                    <span onClick={onClickViewPreviousComments} className={classnames(styles['previous-comment-text'])}>
                        <a>View previous comments</a>
                    </span>
                ) : null
            }

            {
                renderComments(comments)
            }

            {renderCommentInput(0)}
        </div>
    );
};

Comments.propTypes = {
    width: PropTypes.number,
    username: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    numberReplyPerComment: PropTypes.number,
    numberCommentShow: PropTypes.number,
    onReply: PropTypes.func,
    onError: PropTypes.func
};

Comments.defaultProps = defaultProps;

export default Comments;
