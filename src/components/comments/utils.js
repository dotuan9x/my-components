import moment from 'moment';

export const splitStringWithLimitWord = (string, number) => {
    try {
        if (string) {
            return string.split(' ', number).join(' ');
        }

        return string;
    } catch (error) {
        // Error
    }
};

export const formatDateTimeToShow = (time, format = 'YYYY-MM-DD HH:mm:ss') => {
    try {
        if (time) {
            let seconds = moment().diff(moment(time, format), 'seconds');

            if (seconds && +seconds > 0) {
                if (seconds < 60) {
                    return '1 m';

                } else {
                    const minutes = Math.round(seconds / 60);

                    if (minutes > 60) {
                        // Hours
                        const hours = Math.round(minutes / 60);

                        if (hours > 24) {
                            const days = Math.round(hours / 24);

                            return days + ' d';
                        } else {
                            return hours + ' h';
                        }
                    } else {
                        return minutes + ' m';
                    }
                }
            }

            return '1 m';
        }
    } catch (error) {
        // Error
    }
};
